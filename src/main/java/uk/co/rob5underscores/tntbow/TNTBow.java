package uk.co.rob5underscores.tntbow;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

public final class TNTBow extends JavaPlugin implements CommandExecutor, Listener {

    public static Logger bowLogger;

    private List<UUID> chargedFire;

    @Override
    public void onEnable() {
        // Plugin startup logic
        bowLogger = Bukkit.getLogger();
        chargedFire = new ArrayList<>();
        this.getCommand("tntbow").setExecutor(this);
        Bukkit.getServer().getPluginManager().registerEvents(this, this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("tntbow")){
            //perm check?
            if(sender instanceof Player){
                giveBow((Player) sender);
            } else {
                //console sender
            }
        }
        return false;
    }


    @EventHandler
    public void bowFire(EntityShootBowEvent e){
        ProjectileSource shooter = e.getEntity();
        if(shooter instanceof Player){
            if(e.getForce() == 1.0F){
                //max draw
                Optional<Integer> bowUses = getBowUses(e.getBow().getItemMeta());
                if(bowUses.isPresent()) {
                    if(bowUses.get()==3) {
                        chargedFire.add(((Player) shooter).getUniqueId());
                    } else {
                        //normal fire - bow is not full health
                        ((Player) shooter).sendMessage("You could not perform a big explosion as your bow is not fully charged!");
                    }
                }
            }
        }
    }

    @EventHandler
    public void arrowLand(ProjectileHitEvent e){
        ProjectileSource shooter = e.getEntity().getShooter();
        bowLogger.info("[DEBUG] Hit Event");
        if(shooter instanceof Player){
            bowLogger.info("[DEBUG] Player Shot");
            Player pShooter =  (Player)shooter;
            ItemStack bow = pShooter.getItemInHand();
            if(bow.getType().equals(Material.BOW)){
                bowLogger.info("[DEBUG] Arrow landed that was shot by a player from a bow!");
                ItemMeta bowMeta = bow.getItemMeta();
                if(isTNTBow(bowMeta)){
                    bowLogger.info("[DEBUG] Bow name is TNT BOW");
                    if(getBowUses(bowMeta).isPresent()) {
                        Integer bowUses = getBowUses(bowMeta).get();
                        if (tntCheck(pShooter)) {
                            if (bowUses > 1) {
                                if (chargedFire.contains(pShooter.getUniqueId())) {
                                    bowLogger.info("[DEBUG] BIG BOOM");
                                    //big boom
                                    chargedFire.remove(pShooter.getUniqueId());
                                    destroyBow(pShooter, bow);
                                    boom(e.getEntity().getLocation(), true);
                                } else {
                                    bowLogger.info("[DEBUG] NORMAL BOOM");
                                    boom(e.getEntity().getLocation(), false);
                                    //normal boom
                                    if (bowUses == 1) {
                                        bowLogger.info("[DEBUG] Bow uses 1 - destroy");
                                        //destroy bow
                                        destroyBow(pShooter, bow);
                                    } else {
                                        bowLogger.info("[DEBUG] Bow uses not 1 - reduce");
                                        Optional<ItemStack> itemStackOptional = reduceBowCharge(bow);
                                        if(itemStackOptional.isPresent()) {
                                            pShooter.getInventory().setItemInHand(itemStackOptional.get());
                                        } else {
                                            bowLogger.info("[ERROR] meta error");
                                        }
                                    }
                                }
                            } else {
                                //error - bow should have already been destroyed
                                destroyBow(pShooter, bow);
                            }
                        }
                    } else {
                        pShooter.sendMessage("No TNT Ammo");
                    }
                }
            }
        }
    }

    private void giveBow(Player player){
        ItemStack itemStack = new ItemStack(Material.BOW, 1);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("TNT Bow (3 uses)");
        itemStack.setItemMeta(itemMeta);
        player.getInventory().addItem(itemStack);
        player.sendMessage("Here is your bow!");
    }

    private void boom(Location location, boolean big){
        if(big) {
            location.getWorld().createExplosion(location, 12.0F);
        } else {
            location.getWorld().createExplosion(location, 4.0F);

        }
    }

    private boolean tntCheck(Player player){
        for(ItemStack items : player.getInventory().getContents()){
            if(items !=null) {
                if (items.getType() != null && items.getType().equals(Material.TNT)) {
                    if (items.getAmount() > 1) {
                        items.setAmount(items.getAmount() - 1);
                    } else {
                        player.getInventory().remove(items);
                    }
                    return true;
                }
            }
        }

        return false;
    }

    private void destroyBow(Player player, ItemStack bow){
        //might remove multiple of the same bow?
        player.getInventory().remove(bow);
        player.updateInventory();
    }

    private Optional<ItemStack> reduceBowCharge(ItemStack itemStack){
        ItemMeta itemMeta = itemStack.getItemMeta();
        Optional<Integer> bowUsesOpt = getBowUses(itemMeta);
        if(bowUsesOpt.isPresent()){
            Integer bowUses = bowUsesOpt.get();
            String name = itemMeta.getDisplayName();
            name = name.replace(""+bowUses, bowUses-1+"");
            itemMeta.setDisplayName(name);
            short dura = (short)(itemStack.getDurability()-128);
            itemStack.setDurability(dura);
            itemStack.setItemMeta(itemMeta);
            return Optional.of(itemStack);
        } else {
            bowLogger.warning("Tried to reduce bow charge but could not get bow uses");
        }
        return Optional.empty();
    }

    private boolean isTNTBow(ItemMeta itemMeta) {
        return itemMeta.getDisplayName().contains("TNT Bow");
    }

    private Optional<Integer> getBowUses(ItemMeta itemMeta){
        //this check should be redundant
        if(isTNTBow(itemMeta)){
            String name = itemMeta.getDisplayName();
            //replace with REGEX
            name = name.substring(name.length() - 8);
            name = name.replace("(", "");
            name = name.replace(" uses)", "");
            return Optional.of(Integer.valueOf(name));
        }
        return Optional.empty();
    }
}
